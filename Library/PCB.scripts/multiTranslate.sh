#!/bin/bash
# Run as source multiTranslate.sh
#
# This script will translate the Zuken-JSON files to KiCAD format.
# It has to be run after script multiConvert.sh, which translates native Zuken format (s-expression) to JSON
# The output will be stored in the KiCAD directory specified in the outdir variable
#
# The output can be visualized directly with KiCAD PCBnew

python3 zuken2kicad.py --path ../../Zuken/cbr0505-A     --outdir ../../cbr0505-A     --fname cbr0505-A     --flipY True --nVias True --map3d ../../Zuken/map3d.json --power ../../Zuken/power.json --name cbr0505-03 --remap tweak.json
python3 zuken2kicad.py --path ../../Zuken/ctrb0505-A    --outdir ../../ctrb0505-A    --fname ctrb0505-A    --flipY True --nVias True --map3d ../../Zuken/map3d.json --power ../../Zuken/power.json --remap tweak.json
python3 zuken2kicad.py --path ../../Zuken/GR106771-A    --outdir ../../GR106771-A    --fname GR106771-A    --flipY True --nVias True --map3d ../../Zuken/map3d.json --power ../../Zuken/power.json --remap tweak.json
python3 zuken2kicad.py --path ../../Zuken/GR106772-A    --outdir ../../GR106772-A    --fname GR106772-A    --flipY True --nVias True --map3d ../../Zuken/map3d.json --power ../../Zuken/power.json --remap tweak.json
python3 zuken2kicad.py --path ../../Zuken/powersupply-c --outdir ../../powersupply-c --fname powersupply-c --flipY True --nVias True --map3d ../../Zuken/map3d.json --power ../../Zuken/power.json --remap tweak.json --force True
